/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import sic.test.util.SimpleJunit5Launcher;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test0104Java01 {
	/**
	 * Expressions used in Statements to initialize variables of primitive type.
	 * - Variablen mit primitiven Datentypen
	 * - Literale (Konstanten)
	 * - Operatoren `=`, `+` fuer `int` Werte
	 * - Testen der Variablen Inhalte
	 */
	@Test
	void test01() {
		/**
		 * ℤ Ganzzahlen -> Integer Variable, primitiver datentyp `int`
		 * - Hier mit Variablenname `i`
		 * - Hier besetzt die `int` Variable 32-bit oder 4 Byte Speicher
		 *
		 * Genutzte Literale (Konstante)
		 * - `1` Wert 1 als `int`
		 * - `2` Wert 2 als `int`
		 *
		 * Genutzte Operatoren
		 * - `=` wert-zuweisung, assignment
		 * - `+` addition
		 */
		int i = 1; // ℤ ganzzahl, variable (veraenderbar, mutable), initialisiert mit dem Wert `1` (literal)
		i = i + 1; // Erhoehe den Inhalt der Variable `i` um `1` (literal).
		Assertions.assertEquals(2, i); // Test das variable `i` den Inhalt `2` (literal) hat.

		/**
		 * ℤ Ganzzahlen -> Integer Variable, primitiver datentyp `int`
		 * - Hier mit Variablenname `j`
		 * - ...
		 */
		int j; // Undefiniert, nicht initialisiert.
		j = 1; // Weise der Variable den Wert `1` (literal) zu.
		j = j + 1; // Erhoehe den Wert der Variable `j` um `1` (literal)
		Assertions.assertEquals(2, j); // Test das variable `j` den Inhalt `2` (literal) hat.
	}

    /**
     * Block-Statements und Lebensbereich lokaler Variablen
     */
    @Test
    void test02_3_block_statement() { // Block-0 Anfang
        // Block-1 Anfang
        {
            final int i = 1; // Neue variable `i` mit dem Wert `1` initialisiert
            Assertions.assertEquals(1, i);

            // Block-2 Anfang
            {
                final int j = 2; // Neue variable `j` mit dem Wert `2` initialisiert
                final int k = i + j; // Neue variable `k` mit `i+j` initialisiert
                Assertions.assertEquals(3, k);
            }
            // Block-2 Ende
            // Variablen `j` und `k` existieren nicht mehr!
        }
        // Block-1 Ende
        // Variable `i` existiert nicht mehr!
        // i = i + 1; FEHLER, `i` gibts nicht mehr

        // Neue verschachtelte Bloecke mit neuen Variablen & Lebensbereich
        {
            final int i = 2;
            {
                final int j = 3;
                final int k = i + j;
                Assertions.assertEquals(5, k);
            }
        }
    } // Block-0 Ende

	/**
	 * Primitive Datentypen und Literale (Konstante)
	 */
	@Test
	void test03_1_1_literals() {
		final boolean b0 = false; // boolean-value literal
		final byte o0 = 0b0010; 	// bit-value literal -> decimal 2
		final char c0 = 'C';    	// UTF-16 character value literal
        final char c1 = '0';        // UTF-16 character value literal
		final short s0 = 0x0A;  	// hex-value literal -> decimal 10
		final int i0 = 100;     	// int-value literal (default)
		final long l0 = 1000L;  	// long-value literal
		final float f0 = 3.14f; 	// float-value literal
		final double d0 = 2.14; 	// double--value literal (default)
		Assertions.assertEquals(false, b0);
		Assertions.assertEquals((byte)2, o0);
		Assertions.assertEquals('C', c0);
		Assertions.assertEquals(0x30, c1);
		Assertions.assertEquals(10, s0);
		Assertions.assertEquals(100, i0);
		Assertions.assertEquals(1000, l0);
		Assertions.assertEquals(3.14f, f0, Float.MIN_VALUE); // Test value mit erlaubter Fehler-Toleranz Float.MIN_VALUE
		Assertions.assertEquals(2.14, d0, Double.MIN_VALUE); // Test value mit erlaubter Fehler-Toleranz Double.MIN_VALUE
	}

	/**
	 * Primitive Datentypen und Literale (Konstante)
	 */
	@Test
	void test03_1_2_immutable() {
		// Unveraenderbare (Immutable) Variablen, i.e. Konstante
		final int const_i0 = 1;
		// const_i0 = const_i0 + 1; // FEHLER
		Assertions.assertEquals(1, const_i0);
	}

	/**
	 * Binary-Operatoren der 4 Grundrechenarten und Modulo (Divisionsrest) anhand des primitiven Datentyps `int`
	 */
	@Test
	void test03_1_3_a_grundrechenarten() {
		// Addition
		{
			{
				Assertions.assertEquals(3, 1+2);
			}
			{
				final int i = 6; // positiver Wert
				final int j = 2; // positiver Wert
				final int k = i + j;
				Assertions.assertEquals(8, k);
			}
			{
				final int i = +6; // positiver Wert
				final int j = -2; // negativer Wert!!
				final int k = i + j;
				Assertions.assertEquals(4, k);
			}
		}
		// Subtraktion
		{
			final int i = 6;
			final int j = 2;
			final int k = i - j;
			Assertions.assertEquals(4, k);
		}
		// Multiplikation
		{
			final int i = 6;
			final int j = 2;
			final int k = i * j;
			Assertions.assertEquals(12, k);
		}
		// Division
		{
		    {
    			final int i = 6;
    			final int j = 2;
    			final int k = i / j;
    			Assertions.assertEquals(3, k);
		    }
            {
                final int i = 7;
                final int j = 2;
                final int k = i / j; // as real number: 7/2=3.5, but integer simply cuts off the floating point
                Assertions.assertEquals(3, k);
            }
		}
		// Modulo (Divisionsrest)
		{
			{
				final int i = 6;
				final int j = 2;
				final int k = i % j;
				final int l = i - ( i / j ) * j; // Modulo definition, i.e. Divisionsrest
				Assertions.assertEquals(l, k);
				Assertions.assertEquals(0, k);
			}
			{
				final int i = 7;
				final int j = 2;
				final int k = i % j;
				final int l = i - ( i / j ) * j; // Modulo definition, i.e. Divisionsrest
				Assertions.assertEquals(l, k);
				Assertions.assertEquals(1, k);
			}
		}
	}

	/**
	 * Unary-Operatoren (1 Argument) anhand des primitiven Datentyps `int`
	 */
	@Test
	void test03_1_3_b_unary_post_prefix() {
		// Prefix erhoehe (increment) und veringere (decrement)
		{
			int i = 6;
			++i;
			Assertions.assertEquals(7, i);
			--i;
			Assertions.assertEquals(6, i);

			// !!!!
			Assertions.assertEquals(6, i);   // Inhalt von `i` ist `6`
			Assertions.assertEquals(7, ++i); // Inhalt von `i` wird erhoeht, dann zurueckgegeben!
			Assertions.assertEquals(7, i);   // Selber Wert
		}

		// Postfix erhoehe (increment) und veringere (decrement)
		{
			int i = 6;
			i++;
			Assertions.assertEquals(7, i);
			i--;
			Assertions.assertEquals(6, i);

			// !!!!
			Assertions.assertEquals(6, i);   // Inhalt von `i` ist `6`
			Assertions.assertEquals(6, i++); // Inhalt von `i` wird zurueckgegeben, dann erhoeht!
			Assertions.assertEquals(7, i);   // Nun ist der Inhalt von `i`erhoeht
		}
	}

	/**
	 * Zuweisungs-Operatoren inklusive der 4 Grundrechenarten anhand des primitiven Datentyps `int`
	 */
	@Test
	void test03_1_3_c_zuweisung() {
		// Einfache Zuweisung
		{
			int i = 6;
			Assertions.assertEquals(6, i);
			i = 7;
			Assertions.assertEquals(7, i);
		}

		// Addition-Zuweisung
		{
			int i = 6;
			i += 4;
			Assertions.assertEquals(10, i);
		}
		// Subtraktion-Zuweisung
		{
			int i = 6;
			i -= 4;
			Assertions.assertEquals(2, i);
		}
		// Multiplikation-Zuweisung
		{
			int i = 6;
			i *= 4;
			Assertions.assertEquals(24, i);
		}
		// Division-Zuweisung
		{
			int i = 6;
			i /= 2;
			Assertions.assertEquals(3, i);
		}
		// Modulo-Zuweisung
		{
			{
				int i = 6;
				i %= 2;
				Assertions.assertEquals(0, i);
			}
			{
				int i = 7;
				i %= 2;
				Assertions.assertEquals(1, i);
			}
		}
	}
	/**
	 * Operatoren der logischen Vergleiche anhand des primitiven Datentyps `int`
	 */
	@Test
	void test03_1_3_d_vergleich() {
		// Gleichheit (equality)
		{
			final int i = 8;
			final int j = 8;
			final int k = 9;
			Assertions.assertEquals(true,  i == j);
			Assertions.assertEquals(false, i != j);

			Assertions.assertEquals(false, i == k);
			Assertions.assertEquals(true,  i != k);
		}
		// Relational
		{
			final int i = 8;
			final int j = 8;
			final int k = 9;
			Assertions.assertEquals(false, i <  j);
			Assertions.assertEquals(true,  i <= j);
			Assertions.assertEquals(true,  i >= j);
			Assertions.assertEquals(false, i >  k);

			Assertions.assertEquals(true,  i <  k);
			Assertions.assertEquals(true,  i <= k);
			Assertions.assertEquals(false, i >= k);
			Assertions.assertEquals(false, i >  k);
		}
	}

	/**
	 * Operatoren der logischen Verknuepfung anhand des primitiven Datentyps `int` und `boolean`
	 */
	@Test
	void test03_1_3_e_logisch() {
		// Logisch-Und (and) als auch Logisch-Oder (or)
		{
			final int i = 8;
			final int j = 8;
			final int k = 9;
			final boolean b0 = i==j;
			final boolean b1 = i==k;

			Assertions.assertEquals(true,   b0 && !b1);
			Assertions.assertEquals(false, !b0 ||  b1);

			Assertions.assertEquals(true,  i == j && i != k);
			Assertions.assertEquals(false, i != j || i == k);
		}
	}

    /**
     * Integer overflow aware addition returning true if overflow occurred,
     * otherwise false having the result stored in res.
     *
     * Implementation follows API of [GCC Integer Overflow Builtins](https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html).
     *
     * @param a operand a
     * @param b operand b
     * @param res storage for result
     * @return true if overflow, otherwise false
     */
    static boolean add_overflow(final short a, final short b, final short res[/*1*/])
    {
        // overflow:  a + b > R+ -> a > R+ - b, with b >= 0
        // underflow: a + b < R- -> a < R- - b, with b < 0
        if ( ( b >= 0 && a > Short.MAX_VALUE - b ) ||
             ( b  < 0 && a < Short.MIN_VALUE - b ) )
        {
            return true;
        } else {
            res[0] = (short)(a + b);
            return false;
        }
    }

    /**
     * Primitive data types incl. under- and overflow.
     */
    @Test
    void test03_1_4_primitive_underoverflow() {
        // trigger (ausloesung) short over- and underflow: no practical use-case
        {
            final short one = 1;
            short i = Short.MAX_VALUE;
            i = (short)(i + one); // overflow
            Assertions.assertEquals(Short.MIN_VALUE, i);

            i = Short.MIN_VALUE;
            i = (short)(i - one); // underflow
            Assertions.assertEquals(Short.MAX_VALUE, i);
        }
        // trigger short overflow: no practical use-case
        {
            final short a = Short.MAX_VALUE - 10;
            final short b = 11; // a + b -> overflow
            short c;
            {
                // How to make this safe??
                c = (short)(a + b);
            }
        }
        // Safe math using add_overflow()
        // similar to [GCC Integer Overflow Builtins](https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html),
        // i.e. detecting under- and overflow for add operation on two integer.
        {
            final short res[] = { 0 }; // array holding one short value
            Assertions.assertEquals(false, add_overflow((short) 0,       (short) 0, res));
            Assertions.assertEquals((short)0, res[0]);

            res[0] = 0;
            Assertions.assertEquals(false, add_overflow(Short.MAX_VALUE, (short) 0, res));
            Assertions.assertEquals(Short.MAX_VALUE, res[0]);

            res[0] = 0;
            Assertions.assertEquals(true,  add_overflow(Short.MAX_VALUE, (short) 1, res));
            Assertions.assertEquals((short)0, res[0]);

            res[0] = 0;
            Assertions.assertEquals(false, add_overflow(Short.MAX_VALUE, (short)-1, res));
            Assertions.assertEquals(Short.MAX_VALUE-1, res[0]);

            res[0] = 0;
            Assertions.assertEquals(false, add_overflow(Short.MIN_VALUE, (short) 1, res));
            Assertions.assertEquals(Short.MIN_VALUE+1, res[0]);

            res[0] = 0;
            Assertions.assertEquals(true,  add_overflow(Short.MIN_VALUE, (short)-1, res));
            Assertions.assertEquals((short)0, res[0]);

            res[0] = 0;
            Assertions.assertEquals(true,  add_overflow((short) -1,Short.MIN_VALUE, res));
            Assertions.assertEquals((short)0, res[0]);
        }
    }

    public static class Test03A implements Comparable<Test03A> {
        public static final int ONE = 1; // global immutable (constant)
        public static int global_var01 = 1; // explicit initialization (init)
        public static int global_var02; // default initialization

        public int field_member_01 = 1; // explicit initialization (init)
        public int field_member_02;     // default initialization (init)

        /** Default Constructor */
        public Test03A() {
            // keep explicit field_member_01
            // and default field_member_02 initialization (see above)
        }

        /**
         * Custom Constructor 1
         * @param a init value for field_member_01
         * @param b init value for field_member_02
         */
        public Test03A(final int a, final int b) {
            field_member_01 = a;
            field_member_02 = b;
        }
        public void increment() {
            ++field_member_01;
            ++field_member_02;
        }
        public void reset() {
            field_member_01 = 1;
            field_member_02 = 0;
        }
        @Override
        public boolean equals(final Object o) {
            if( this == o ) {
                return true;
            }
            if( !(o instanceof Test03A) ) {
                return false;
            }
            final Test03A p = (Test03A)o;
            return this.field_member_01 == p.field_member_01 &&
                   this.field_member_02 == p.field_member_02;
        }
        @Override
        public int hashCode() {
            // 31 * x == (x << 5) - x
            int hash = 31 + field_member_01;
            hash = ((hash << 5) - hash) + field_member_02;
            return hash;
        }
        @Override
        public String toString() {
            return "Test03A[01 "+field_member_01+", 02 "+field_member_01+"]";
        }

        @Override
        public int compareTo(final Test03A o) {
            final int s1 = field_member_01 + field_member_02;
            final int s2 = o.field_member_01 + o.field_member_02;
            if( s1 == s2 ) {
                return 0;
            }
            if( s1 < s2 ) {
                return -1;
            }
            return 1;
        }
    }

    @Test
    void test03_2_a_class() {
        {
            final Test03A o1 = new Test03A();
            Assertions.assertEquals(1, o1.field_member_01);
            Assertions.assertEquals(0, o1.field_member_02);

            final Test03A o2 = new Test03A();
            Assertions.assertEquals(1, o2.field_member_01);
            Assertions.assertEquals(0, o2.field_member_02);

            Assertions.assertEquals(o1, o2);
            Assertions.assertEquals(0, o1.compareTo(o2));

            o1.increment();
            Assertions.assertEquals(2, o1.field_member_01);
            Assertions.assertEquals(1, o1.field_member_02);
            Assertions.assertNotEquals(o1, o2);
            Assertions.assertEquals( 1, o1.compareTo(o2));
            Assertions.assertEquals(-1, o2.compareTo(o1));

            o1.reset();
            Assertions.assertEquals(o1, o2);
            Assertions.assertEquals(0, o1.compareTo(o2));
        }
        {
            final Test03A o1 = new Test03A(2, 3);
            Assertions.assertEquals(2, o1.field_member_01);
            Assertions.assertEquals(3, o1.field_member_02);

            final Test03A o2 = new Test03A(2, 3);
            Assertions.assertEquals(2, o2.field_member_01);
            Assertions.assertEquals(3, o2.field_member_02);

            Assertions.assertEquals(o1, o2);
            Assertions.assertEquals(0, o1.compareTo(o2));
        }
    }

    @Test
    void test03_5_arrays() {
        final int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        Assertions.assertEquals(1+10, a[0] + a[9]);
        Assertions.assertEquals(2, a[1]);
        Assertions.assertEquals(9, a[8]);
        Assertions.assertEquals(10, a.length); // length of the array == 10!
    }

	/**
	 * Programmfluss-Statement: Branches (if, switch-case, conditional-op)
	 */
	@SuppressWarnings("unused")
    @Test
	void test04_1_branch() {
	    // branches: if
	    {
	    	int state = -1;
	        final int a = 0;

	        // Note that we place the immutable literal (r-value) on the left-side
	        // of the equality operation '0 == a',
	        // which avoids accidental assignment of a mutable l-value if typo 'a = 0'.
	        if( 0 == a ) {
	            // executed if 'a' contains '0'
	            state = 1;
	            Assertions.assertTrue(true);
	        } else if( 1 == a ) {
	            // executed if 'a' contains '1'
	        	Assertions.assertTrue(false); // unreachable
	        }

	        Assertions.assertEquals(1, state);
	        state = -1;

	        // Note that the expression `0 == a` is a boolean expression,
	        // i.e. resolved to either `true` or `false`.
	        final boolean b0 = 0 == a;
	        final boolean b1 = 1 == a;

	        // Note that the expression for `if` and `while` are boolean expressions.
	        //
	        // Below we use the pre-computed boolean results.
	        if( b0 ) {
	            // executed if b0 is true, i.e. 'a' contains '0'
	        	state = 1;
	        	Assertions.assertTrue(true);
	        } else if( b1 ) {
	            // executed if b1 is true, i.e. 'a' contains '1'
	        	Assertions.assertTrue(false); // unreachable
	        }

	        Assertions.assertEquals(1, state);
	    }

	    // branches: switch
	    {
	    	int state = -1;
	        final int a = 0;

	        switch( a ) {
	            case 0:
	                // executed if 'a' contains '0'
	            	state = 1;
	            	Assertions.assertTrue(true);
	                break; // ends code for this case
	            case 1:
	                // executed if 'a' contains '0'
	                {
	                    // use an inner block-statement to allow local case resources
	                    final int v = 1;
	                    System.out.println("branch1."+v);
	                }
	                Assertions.assertTrue(false); // unreachable
	                break; // ends code for this case
	            case 2:
	                // executed if 'a' contains '2'
	                // and falls through to default case code
	                // [[fallthrough]];
	            	Assertions.assertTrue(false); // unreachable
	            default:
	                // executed if none of the above cases matches
	            	Assertions.assertTrue(false); // unreachable
	                break; // ends code for this case
	        }
	        Assertions.assertEquals(1, state);
	    }

	    // branches: conditional operator
	    {
	        final int a = 0;

	        // initialized with '0' if 'a' contains '0', otherwise initialized with '1'
	        final char c = ( 0 == a ) ? '0' : '1';

	        Assertions.assertEquals('0', c);
	    }
	}

	/**
	 * Programmfluss-Statement: Loops (while, do-while, for, break)
	 */
	@Test
	void test04_2_loops() {
	    final int loop_count = 3;
	    // Same loop as while
	    {
	        // while loop, an exploded for-loop (see below)
	    	int v=10;
	        int i=0;              /* instantiation and initialization of loop variable */
	        while( i < loop_count /* while condition */ ) {
	            ++v;
	            i = i + 1;        /* tail expression */
	        }
	        Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // Same loop as do-while
	    {
	        // do-while loop - executed at least once
	    	int v=10;
	        int i=0;              /* instantiation and initialization of loop variable */
	        do {
	            ++v;
	            i = i + 1;        /* tail expression */
	        } while( i < loop_count /* while condition */ );
	        Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // Same loop as for (1)
	    {
	    	int v=10;
	        int i;                /* instantiation of loop variable*/
	        for(i=0               /* initialization of loop variable*/;
	            i<loop_count      /* while condition */;
	            ++i               /* tail expression */)
	        {
	        	++v;
	        }
	        // `i` is still in scope!
	        Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // Same loop as for (2)
	    {
	    	int v=10;
	        /* instantiation and initialization of loop variable; while condition; tail expression */
	        for(int i=0; i<loop_count; ++i) {
	            ++v;
	        }
	        // `i` is out of scope!
	        // Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // Using break within a loop
	    {
	    	int v=10;
	        int i=0;              /* instantiation and initialization of loop variable */
	        while( true /* while condition: forever */ ) {
	            if( i >= loop_count ) {
	                break; // exit loop
	            }
	        	++v;
	            i = i + 1;        /* tail expression */
	        }
	        Assertions.assertEquals(loop_count, i);
	        Assertions.assertEquals(10+loop_count, v);
	    }
	    // loop through an array
	    {
	        // Calculate the sum of all array elements
	        final int a[] = { 1, 2, 3, 4 };
	        int sum = 0;
	        for(int i=a.length-1; i>=0; --i) {
	            sum += a[i];
	        }
	        Assertions.assertEquals(10, sum);
	    }
	}

	static int addiere(final int a, final int b) {
		return a+b;
	}

	@Test
	void test30_method_static() {
		Assertions.assertEquals(5, addiere(2, 3));
	}

	int instanz_var = 0;

	int erhoeheUm(final int a) {
		instanz_var += a;
		return instanz_var;
	}

	@Test
	void test31_method_instanz() {
	    instanz_var = 0;
		Assertions.assertEquals(2, erhoeheUm(2));
		Assertions.assertEquals(2, instanz_var);
		Assertions.assertEquals(5, erhoeheUm(3));
		Assertions.assertEquals(5, instanz_var);
	}

	public static void main(final String[] args) {
        SimpleJunit5Launcher.runTests(Test0104Java01.class);
    }

}
