/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Logik, JUC2 02.01 Logic01
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0201Logik01Aufgabe02 {

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(¬A ∨ ¬B) ∧ (¬A ∨ B) ∧ (A ∨ ¬B)</li>
	 *   <li>(!a || !b) && (!a || b) && (a || !b)</li>
	 * </ul>
	 * Solution is ¬A ∨ ¬B
	 */
	static boolean term01(final boolean a, final boolean b) {
	    // Optimierung ist super, aber: Lesbarkeit versus Optimierung beachten!
	    //
		// 0: (!a || !b) && (!a || b) && (a || !b);
		// 1: ( !a || (!b && b) )     && (a || !b); // Distributivgesetze ('faktorisiert' -> !a rausgezogen)
		// 2: ( !a || false )         && (a || !b); // Neutralitaetsgesetze: !A ∨ 0 = !A
		// 3:   !a                    && (a || !b); // Distributivgesetze ('ausmultipliziert')
	    // 4: false || ( !a && !b );
	    return !a && !b;
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(A ∧ B) ∨ (A ∧ C) ∨ (B ∧ ¬C)</li>
	 *   <li>(a && b) || (a && c) || (b && !c)</li>
	 * </ul>
	 */
	static boolean term02(final boolean a, final boolean b, final boolean c ) {
        // (a && b) || (a && c) || (b && !c)
	    // (a && ( b || c)) || (b && !c)
	    // 2: a && ( b || !c)
	    // 2.1: ( b && a) || b && !c) || ( a && c)
	    // 2.2: ( b && a || !c ) || ( a && c)
	    // 3: ( True && True) || (True && True) || ( True && False)
	    // 4: True || True || False
	    // ( a && c) == ( a && b) || ( b && !c)
	    //  ( a && b) || ( a && c)
	    return ( a && c) || ( b && !c);

	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(A ∧ ¬B) ∨ (A ∧ ¬B ∧ C)</li>
	 *   <li>(a && !b) || (a && !b && c)</li>
	 * </ul>
	 */
	static boolean term03(final boolean a, final boolean b, final boolean c) {
	    return a && !b; // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(A ∨ ¬(B ∧ A)) ∧ (C ∨ (D ∨ C))</li>
	 *   <li>(a || !(b && a)) && (c || (d || c))</li>
	 * </ul>
	 */
	static boolean term04(final boolean a, final boolean b, final boolean c, final boolean d) {
	    // 0: (a || !(b && a))  && (c || (d || c));
	    // 1: (a || (!b || !a)) && (c || (d || c)) // De Morgan
	    //
	    //a2: ((a || !b) || (a || !a)) && (c || (d || c)) // Distributivgesetz ('ausmultiplizieren')
	    //a3: ((a || !b) || true) && (c || (d || c)) // Komplementaer
	    //a4:                        (c || (d || c)) // Extremalgesetz
	    //a5:                        c || (d || c)   //
        //a6:                        c || d || c   // Assoziations..
	    //a7:                        c || d        // ******
	    //
        //b2: (a || !b || !a) && (c || d || c)  // Assoziations..
        //b3: (a || !a || !b ) && (c || d)  // Assoziations..
        //b3: (true    || !b ) && (c || d )  // Komplementaergesetze
        //b4:                     (c || d )  // Neutralitaetsgesetze ****
	    //
		return c || d;
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(¬(A ∧ B) ∨ ¬C) ∧ (¬A ∨ B ∨ ¬C)</li>
	 *   <li>(!(a && b) || !c) && (!a || b || !c)</li>
	 * </ul>
	 */
	static boolean term05(final boolean a, final boolean b, final boolean c) {
		// 1:(!(a && b) || !c) && (!a || b || !c)
	    // 2: ( !a || !b || !c) && ( !a || b || !c) -----> De Moran^sche Gesetze
	    // 2.1: ( !a || !c) || !b) && ( !a || !c) || b)
	    // 2.1.1: ( ( !a || !c) || !b) && ( b)
	    // 2.2: ( !a || !c) || ( !b && b)
	    // 3: ( !a || !c ) && ( !b || b)
	    // 4: ! a || !c
	    return ! a || !c ;  // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>¬(¬(A ∧ B) ∨ C) ∨ (A ∧ C)</li>
	 *   <li>!(!(a && b) || c) || (a && c)</li>
	 * </ul>
	 */
	static boolean term06(final boolean a, final boolean b, final boolean c) {
		// 1: !(!(a && b) || c) || (a && c)
	    // 2: ( a && b && !c) || a && c) -----> De Morgan^sche Gesetz / Doppelnegationsgesetze
	    // 3: a && b || a && c ----> Absorpitionsgesetze
	    // 4: a && ( b || c) ----> Distributivgesetz
	    return  a && ( b || c) ; // TODO: Simplify
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>(A ∨ B) ∧ (¬A ∨ B) ∧ (A ∨ ¬B) ∧ (¬A ∨ ¬B)</li>
	 *   <li>(a || b) && (!a || b) && (a || !b) && (!a || !b)</li>
	 * </ul>
	 */
	static boolean term07(final boolean a, final boolean b) {
		// 1: (a || b) && (!a || b) && (a || !b) && (!a || !b)
	    // 2: ( !a || b) && (!a || !b)
	    // 3: ( false || true) && (False || True)
	    // 4: False && False
	    return  false; // TODO: Simplify;
	}

	/**
	 * Returns the simplified term of:
	 * <ul>
	 *   <li>A ∨ (¬B ∧ ¬(A ∨ ¬B ∨ C))</li>
	 *   <li>a || (!b && !(a || !b || c))</li>
	 * </ul>
	 */
	static boolean term08(final boolean a, final boolean b, final boolean c) {
		// 1: a || (!b && !(a || !b || c))
	    // 2: a || ( !b && !a || !c)
	    // 3: a || !b && !a || !c
	    // 4: a || false && false || false
	    // 5: a || false
	    return a || false; // TODO: Simplify
	}

	@Test
	void test00() {
		// bool'sche variable, logik, wahr (true, 1) oder falsch (false, 0)
		final boolean T = true;  // immutable (konstante) booleans
		final boolean F = false; // ..

		Assertions.assertEquals(T, term01(F, F));
		Assertions.assertEquals(F, term01(F, T));
		Assertions.assertEquals(F, term01(T, F));
		Assertions.assertEquals(F, term01(T, T));


		Assertions.assertEquals(F, term03(F, F, F));
		Assertions.assertEquals(F, term03(F, F, T));
		Assertions.assertEquals(F, term03(F, T, F));
		Assertions.assertEquals(F, term03(F, T, T));
		Assertions.assertEquals(T, term03(T, F, F));
		Assertions.assertEquals(T, term03(T, F, T));
		Assertions.assertEquals(F, term03(T, T, F));
		Assertions.assertEquals(F, term03(T, T, T));

		Assertions.assertEquals(F, term04(F, F, F, F));
		Assertions.assertEquals(T, term04(F, F, F, T));
		Assertions.assertEquals(T, term04(F, F, T, F));
		Assertions.assertEquals(T, term04(F, F, T, T));
		Assertions.assertEquals(F, term04(F, T, F, F));
		Assertions.assertEquals(T, term04(F, T, F, T));
		Assertions.assertEquals(T, term04(F, T, T, F));
		Assertions.assertEquals(T, term04(F, T, T, T));
		Assertions.assertEquals(F, term04(T, F, F, F));
		Assertions.assertEquals(T, term04(T, F, F, T));
		Assertions.assertEquals(T, term04(T, F, T, F));
		Assertions.assertEquals(T, term04(T, F, T, T));
		Assertions.assertEquals(F, term04(T, T, F, F));
		Assertions.assertEquals(T, term04(T, T, F, T));
		Assertions.assertEquals(T, term04(T, T, T, F));
		Assertions.assertEquals(T, term04(T, T, T, T));

		Assertions.assertEquals(T, term05(F, F, F));
		Assertions.assertEquals(T, term05(F, F, T));
		Assertions.assertEquals(T, term05(F, T, F));
		Assertions.assertEquals(T, term05(F, T, T));
		Assertions.assertEquals(T, term05(T, F, F));
		Assertions.assertEquals(F, term05(T, F, T));
		Assertions.assertEquals(T, term05(T, T, F));
		Assertions.assertEquals(F, term05(T, T, T));

		Assertions.assertEquals(F, term06(F, F, F));
		Assertions.assertEquals(F, term06(F, F, T));
		Assertions.assertEquals(F, term06(F, T, F));
		Assertions.assertEquals(F, term06(F, T, T));
		Assertions.assertEquals(F, term06(T, F, F));
		Assertions.assertEquals(T, term06(T, F, T));
		Assertions.assertEquals(T, term06(T, T, F));
		Assertions.assertEquals(T, term06(T, T, T));

		Assertions.assertEquals(F, term07(F, F));
		Assertions.assertEquals(F, term07(F, T));
		Assertions.assertEquals(F, term07(T, F));
		Assertions.assertEquals(F, term07(T, T));

		Assertions.assertEquals(F, term08(F, F, F));
		Assertions.assertEquals(F, term08(F, F, T));
		Assertions.assertEquals(F, term08(F, T, F));
		Assertions.assertEquals(F, term08(F, T, T));
		Assertions.assertEquals(T, term08(T, F, F));
		Assertions.assertEquals(T, term08(T, F, T));
		Assertions.assertEquals(T, term08(T, T, F));
		Assertions.assertEquals(T, term08(T, T, T));
	}
}
