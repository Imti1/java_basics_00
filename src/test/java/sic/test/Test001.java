package sic.test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test001 {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test00() {
		// ℤ Ganzzahlen -> Integer Variable
		// Datentyp `int`
		// Variablenname `i`
		// Variable besetzt Speicher, ausreichend fuer den Datentyp
		//
		// Operationen
		// - `=` wert-zuweisung, assignment
		// - `+` addition
		int i = 1; // ℤ ganzzahl, variable (veraenderbar, mutable), initialisiert mit dem Wert `1`
		i = i + 1; // erhoehe den Inhalt der Variable `i` um eins.
		Assertions.assertEquals(2, i);
		
		int j; // undefiniert
		j = 1;
		j = j + 1;
		Assertions.assertEquals(2, j);
	}
	
	@Test
	void test01() {
		// Ganzzahlen -> Integer Konstante 
		final int i = 1; // ℤ ganzzahl, konstante (nicht veraenderbar, immutable), initialisiert mit dem Wert `1` 
		Assertions.assertEquals(1, i);
	}

	@Test
	void test02() {		
		final int i = 1;
		final int j = 2;
		final int k = i + j; // Addieren (Grundrechenarten): Integer Operation
		Assertions.assertEquals(3, k);
	}
}
