/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package sic.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Java Grundelemente, JUC2 01.04 Java01
 * <p>
 * Basic integer arithmetic part 2b: Integer Division _and_ comparable Fraction + DivResult classes.
 * </p>
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0104Java01Aufgabe02b {
	public static class Fraction implements Comparable<Fraction> {
		public final int numerator;
		public final int denominator;

		public Fraction(final int num, final int den) {
		    if( 0 == den ) {
		        throw new java.lang.ArithmeticException("denominator is zero -> undefined operation: "+toString());
		    }
			numerator = num;
			denominator = den;
		}

		@Override
        public boolean equals(final Object o) {
			if( this == o ) {
				return true;
			}
			if( !(o instanceof Fraction) ) {
				return false;
			}
			final Fraction of = (Fraction)o;
			return numerator == of.numerator &&
				   denominator == of.denominator;
		}

		@Override
        public int compareTo(final Fraction o) {
		    if( denominator < o.denominator ) {
		        Assertions.assertNotEquals(this, o);
		        return 1;
		    }
		    if( denominator > o.denominator ) {
		        Assertions.assertNotEquals(this, o);
		        return -1;
		    }
		    if( numerator > o.numerator ) {
		        Assertions.assertNotEquals(this, o);
		        return 1;
		    }
            if( numerator < o.numerator ) {
                Assertions.assertNotEquals(this, o);
                return -1;
            }
            Assertions.assertEquals(this, o);
            return 0;
		}

		@Override
        public String toString() { return numerator+"/"+denominator; }
	}

    @Test
    void test10_Fractions() {
        Assertions.assertThrows(java.lang.ArithmeticException.class, () -> { new Fraction(1, 0); } );

        Assertions.assertEquals(-1, new Fraction(1, 1).compareTo( new Fraction(2, 1) ) );
        Assertions.assertEquals( 0, new Fraction(1, 1).compareTo( new Fraction(1, 1) ) );
        Assertions.assertEquals( 1, new Fraction(2, 1).compareTo( new Fraction(1, 1) ) );
    }

	public static class DivResult implements Comparable<DivResult> {
	    public final int factor;
		public final Fraction remainder;

		public DivResult(final int fac, final Fraction rem) {
			factor = fac;
			remainder = rem;
		}

        @Override
        public boolean equals(final Object o) {
            if( this == o ) {
                return true;
            }
            if( !(o instanceof DivResult) ) {
                return false;
            }
            final DivResult dr = (DivResult)o;
            return factor == dr.factor &&
                   remainder.equals(dr.remainder);
        }

        @Override
        public int compareTo(final DivResult o) {
            if( factor > o.factor ) {
                Assertions.assertNotEquals(this, o);
                return 1;
            }
            if( factor < o.factor ) {
                Assertions.assertNotEquals(this, o);
                return -1;
            }
            return remainder.compareTo(o.remainder);
        }

		@Override
        public String toString() { return factor+" + "+remainder; }
	}

    @Test
    void test11_DivResult() {
        Assertions.assertThrows(java.lang.ArithmeticException.class, () -> { new DivResult(1, new Fraction(1, 0)); } );

        Assertions.assertEquals(-1, new DivResult(1, new Fraction(1, 1)).compareTo( new DivResult(2, new Fraction(1, 1)) ) );
        Assertions.assertEquals( 0, new DivResult(1, new Fraction(1, 1)).compareTo( new DivResult(1, new Fraction(1, 1)) ) );
        Assertions.assertEquals( 1, new DivResult(2, new Fraction(1, 1)).compareTo( new DivResult(1, new Fraction(1, 1)) ) );

        Assertions.assertEquals(-1, new DivResult(1, new Fraction(1, 4)).compareTo( new DivResult(1, new Fraction(1, 2)) ) );
        Assertions.assertEquals(-1, new DivResult(1, new Fraction(1, 2)).compareTo( new DivResult(1, new Fraction(1, 1)) ) );
        Assertions.assertEquals( 1, new DivResult(1, new Fraction(1, 1)).compareTo( new DivResult(1, new Fraction(1, 2)) ) );
        Assertions.assertEquals( 1, new DivResult(1, new Fraction(1, 2)).compareTo( new DivResult(1, new Fraction(1, 4)) ) );
    }

    /**
     * Returns the division result of integer values `a` / `b`.
     * <pre>
     * - a==7, b==2 -> 7 / 2 -> return DivResult { 3, Fraction { 1, 2 } }
     * </pre>
     */
    static DivResult div_a(final int numerator, final int denominator) {
        final int faktor = numerator / denominator;
        final int remainder = numerator % denominator;
        return new DivResult(faktor, new Fraction(remainder, denominator));
    }

	/**
	 * Test fuer {@link #div_a(int, int)}
	 */
	@Test
	void test20_div() {
		Assertions.assertEquals( new DivResult(0, new Fraction(0, 1)), div_a(0, 1));
		Assertions.assertEquals( new DivResult(1, new Fraction(0, 1)), div_a(1, 1));
		Assertions.assertEquals( new DivResult(1, new Fraction(1, 2)), div_a(3, 2));
		Assertions.assertEquals( new DivResult(3, new Fraction(0, 2)), div_a(6, 2));
		Assertions.assertEquals( new DivResult(3, new Fraction(1, 2)), div_a(7, 2));
	}

}
