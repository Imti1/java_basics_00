/**
 * Copyright (c) 2020-2024 Sven Gothel
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.sic.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

public class JarVersion {

    /** See {@link #getImplementationBranch()} */
    public static final Attributes.Name IMPLEMENTATION_BRANCH = new Attributes.Name("Implementation-Branch");
    /** See {@link #getImplementationCommit()} */
    public static final Attributes.Name IMPLEMENTATION_COMMIT = new Attributes.Name("Implementation-Commit");

    private static final String packageNameFAT = "org.sic";

    private final String packageName;
    private final Manifest mf;
    private final int hash;
    private final Attributes mainAttributes;
    private final Set<?>/*<Attributes.Name>*/ mainAttributeNames;

    protected JarVersion(final String packageName, final Manifest mf) {
        if( null != mf ) {
            // use provided valid data
            this.mf = mf;
            this.packageName = packageName;
        } else {
            // try FAT jar file
            final Manifest fatMF = getManifest(JarVersion.class.getClassLoader(), packageNameFAT);
            if( null != fatMF ) {
                // use FAT jar file
                this.mf = fatMF;
                this.packageName = packageNameFAT;
            } else {
                // use faulty data, unresolvable ..
                this.mf = new Manifest();
                this.packageName = packageName;
            }
        }
        this.hash = this.mf.hashCode();
        mainAttributes = this.mf.getMainAttributes();
        mainAttributeNames = mainAttributes.keySet();
    }

    @Override
    public final int hashCode() {
        return hash;
    }

    @Override
    public final boolean equals(final Object o) {
        if (o instanceof JarVersion) {
            return mf.equals(((JarVersion) o).getManifest());
        }
        return false;
    }

    public final Manifest getManifest() {
        return mf;
    }

    public final String getPackageName() {
        return packageName;
    }

    public final String getAttribute(final Attributes.Name attributeName) {
        return (null != attributeName) ? (String) mainAttributes.get(attributeName) : null;
    }

    public final String getAttribute(final String attributeName) {
        return getAttribute(getAttributeName(attributeName));
    }

    public final Attributes.Name getAttributeName(final String attributeName) {
        for (final Iterator<?> iter = mainAttributeNames.iterator(); iter.hasNext();) {
            final Attributes.Name an = (Attributes.Name) iter.next();
            if (an.toString().equals(attributeName)) {
                return an;
            }
        }
        return null;
    }

    /**
     * @return set of type {@link Attributes.Name}, disguised as anonymous
     */
    public final Set<?>/*<Attributes.Name>*/ getAttributeNames() {
        return mainAttributeNames;
    }

    public final String getExtensionName() {
        return this.getAttribute(Attributes.Name.EXTENSION_NAME);
    }

    /**
     * Returns the SCM branch name
     */
    public final String getImplementationBranch() {
        return this.getAttribute(JarVersion.IMPLEMENTATION_BRANCH);
    }

    /**
     * Returns the SCM version of the last commit, e.g. git's sha1
     */
    public final String getImplementationCommit() {
        return this.getAttribute(JarVersion.IMPLEMENTATION_COMMIT);
    }

    public final String getImplementationVersion() {
        return this.getAttribute(Attributes.Name.IMPLEMENTATION_VERSION);
    }

    public final String getSpecificationVersion() {
        return this.getAttribute(Attributes.Name.SPECIFICATION_VERSION);
    }

    public final StringBuilder getFullManifestInfo(final StringBuilder sb) {
        return getFullManifestInfo(getManifest(), sb);
    }

    public StringBuilder getManifestInfo(StringBuilder sb) {
        if(null==sb) {
            sb = new StringBuilder();
        }
        final String nl = System.getProperty("line.separator");
        sb.append("Package: ").append(getPackageName()).append(nl);
        sb.append("Specification Version: ").append(getSpecificationVersion()).append(nl);
        sb.append("Implementation Version: ").append(getImplementationVersion()).append(nl);
        sb.append("Implementation Branch: ").append(getImplementationBranch()).append(nl);
        sb.append("Implementation Commit: ").append(getImplementationCommit()).append(nl);
        return sb;
    }

    public StringBuilder toString(StringBuilder sb) {
        if(null==sb) {
            sb = new StringBuilder();
        }
        return getManifestInfo(sb);
    }

    @Override
    public String toString() {
        return toString(null).toString();
    }

    //
    //
    //

    /**
     * Returns the manifest of the jar which contains the specified extension.
     * The provided ClassLoader is used for resource loading.
     * @param cl A ClassLoader which should find the manifest.
     * @param extension The value of the 'Extension-Name' jar-manifest attribute; used to identify the manifest.
     * @return the requested manifest or null when not found.
     */
    public static Manifest getManifest(final ClassLoader cl, final String extension) {
        return getManifest(cl, new String[] { extension } );
    }

    /**
     * Returns the manifest of the jar which contains one of the specified extensions.
     * The provided ClassLoader is used for resource loading.
     * @param cl A ClassLoader which should find the manifest.
     * @param extensions The values of many 'Extension-Name's jar-manifest attribute; used to identify the manifest.
     *                   Matching is applied in decreasing order, i.e. first element is favored over the second, etc.
     * @return the requested manifest or null when not found.
     */
    public static Manifest getManifest(final ClassLoader cl, final String[] extensions) {
        final Manifest[] extManifests = new Manifest[extensions.length];
        try {
            final Enumeration<URL> resources = cl.getResources("META-INF/MANIFEST.MF");
            while (resources.hasMoreElements()) {
                final Manifest manifest;
            	try( InputStream is = resources.nextElement().openStream() ) {
                    manifest = new Manifest(is);
                }
                final Attributes attributes = manifest.getMainAttributes();
                if(attributes != null) {
                    for(int i=0; i < extensions.length && null == extManifests[i]; i++) {
                        final String extension = extensions[i];
                        if( extension.equals( attributes.getValue( Attributes.Name.EXTENSION_NAME ) ) ) {
                            if( 0 == i ) {
                                return manifest; // 1st one has highest prio - done
                            }
                            extManifests[i] = manifest;
                        }
                    }
                }
            }
        } catch (final IOException ex) {
            throw new RuntimeException("Unable to read manifest.", ex);
        }
        for(int i=1; i<extManifests.length; i++) {
            if( null != extManifests[i] ) {
                return extManifests[i];
            }
        }
        return null;
    }

    public static StringBuilder getFullManifestInfo(final Manifest mf, StringBuilder sb) {
        if(null==mf) {
            return sb;
        }

        if(null==sb) {
            sb = new StringBuilder();
        }

        final String nl = System.getProperty("line.separator");
        final Attributes attr = mf.getMainAttributes();
        final Set<Object> keys = attr.keySet();
        for(final Iterator<Object> iter=keys.iterator(); iter.hasNext(); ) {
            final Attributes.Name key = (Attributes.Name) iter.next();
            final String val = attr.getValue(key);
            sb.append(" ");
            sb.append(key);
            sb.append(" = ");
            sb.append(val);
            sb.append(nl);
        }
        return sb;
    }
}
